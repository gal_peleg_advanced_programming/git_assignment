#include "threads.h"

void I_Love_Threads()
{
	cout << "I Love Threads" << endl;
}
void call_I_Love_Threads()
{
	thread t1(I_Love_Threads);
	t1.join();
}

void printVector(vector<int> primes)
{
	for (unsigned int i = 0; i < primes.size(); i++)
	{
		cout << primes[i] << endl;
	}
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	bool prime = true;
	for (int i = begin; i <= end; i++) // runing threw all range of numbers
	{
		for (int j = 2; j <= i / 2; j++)// checking if number is prime
		{
			if (i % j == 0)
			{
				prime = false;
				break;
			}
		}
		if (prime) // if prime than adds it to vector
		{
			primes.push_back(i);
		}
		prime = true;
	}
}
vector<int> callGetPrimes(int begin, int end)
{
	int time = 0;
	vector<int> primes;
	time = clock();
	thread t1(getPrimes, begin, end, ref(primes));
	t1.join();
	cout << "run for " << clock() - time << "ms" << endl;
	return primes;
}


void writePrimesToFile(int begin, int end, ofstream& file)
{
	vector<int> primes = callGetPrimes(begin, end);
	for (unsigned int i = 0; i < primes.size(); i++)
	{
		file << primes[i] << endl;
	}
}
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	ofstream primes(filePath);
	clock_t start, finish;
	double time_spent;


	start = clock();

	int amount = (end - begin) / N + 1;
	thread* arr = new thread[N];
	for (int i = 0; i < N; i++)
	{
		int new_end = begin + amount * (i + 1) - 1 > end ? end : begin + amount * (i + 1) - 1;
		arr[i] = thread(writePrimesToFile, begin + amount * i, new_end, ref(primes));
	}
	for (int i = 0; i < N; i++)
	{
		arr[i].join();
	}

	finish = clock();
	time_spent = (double)(finish - start) / CLOCKS_PER_SEC;
	cout << begin << "-" << end << setprecision(5) << ": " << time_spent << " seconds" << endl;

	primes.close();
}