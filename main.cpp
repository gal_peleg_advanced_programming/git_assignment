#include "threads.h"

int main()
{
	call_I_Love_Threads();
	
	vector<int> primes1;
	getPrimes(1, 1000, primes1);
	printVector(primes1);  
	vector<int> primes3 = callGetPrimes(93, 289);
	vector<int> primes4 = callGetPrimes(0, 1000);
	vector<int> primes5 = callGetPrimes(0, 10000);
	vector<int> primes6 = callGetPrimes(0, 100000);
	callWritePrimesMultipleThreads(1, 1111, "primes2.txt", 4);
	
	system("pause");
	return 0;
}